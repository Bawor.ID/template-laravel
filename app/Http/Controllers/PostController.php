<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
class PostController extends Controller
{
    public function create()
    {
        return view('posts.create');
    }

    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);
        $query = DB::table('post')->insert([
            "title" => $request["title"],
            "body" => $request["body"]
        ]);
        return redirect('/pertanyaan');
    }
    //TAMPIL DATA
    public function index()
    {
        $post = DB::table('post')->get();
        return view('posts.index', compact('post'));
    }
    //TAMPIL DATA BERDASAR ID
    public function show($id)
    {
        $post = DB::table('post')->where('id', $id)->first();
        return view('posts.show', compact('post'));
    }

    //UPDATE DATA
    public function edit($id)
    {
        $post = DB::table('post')->where('id', $id)->first();
        return view('posts.edit', compact('post'));
    }

    public function update($id, Request $request)
    {
        $request->validate([
            'title' => 'required|unique:post',
            'body' => 'required',
        ]);

        $query = DB::table('post')
            ->where('id', $id)
            ->update([
                'title' => $request["title"],
                'body' => $request["body"]
            ]);
        return redirect('/pertanyaan');
    }

    //DELETE DATA
    public function destroy($id)
    {
        $query = DB::table('post')->where('id', $id)->delete();
        return redirect('/pertanyaan');
    }
}
