<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('adminlte.partials.welcome');
});

Route::get('/data-table', function () {
    return view('adminlte.partials.data-table');
});




Route::get('/pertanyaan/create', 'PostController@create');

Route::post('/pertanyaan', 'PostController@store');

Route::get('/pertanyaan', 'PostController@index');

Route::get('/pertanyaan/{pertanyaan_id}', 'PostController@show');

Route::get('/pertanyaan/{pertanyaan_id}/edit', 'PostController@edit');

Route::put('/pertanyaan/{pertanyaan_id}', 'PostController@update');

Route::delete('/pertanyaan/{pertanyaan_id}', 'PostController@destroy');



